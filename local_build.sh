#!/bin/bash

TARGET_VERSION=$(grep '^TARGET_VERSION=' TARGET_VERSION | cut -d= -f2)
export TARGET_VERSION
./docker_build.sh
unset TARGET_VERSION