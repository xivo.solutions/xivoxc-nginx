#!/bin/bash

ME=$(basename "$0")

verify_configmgt_access_provided(){
    if [[ -z "${NGINX_PLAY_AUTH_TOKEN}" ]] || [[ -z "${NGINX_XIVO_HOST}" ]]; then
        echo "$ME: info: NGINX_PLAY_AUTH_TOKEN or NGINX_XIVO_HOST is empty, exiting"
        exit 1
    fi
}

fetch_json_mds_list(){
    if ! json_mds=$(curl -sk -X GET -H "X-Auth-Token:${NGINX_PLAY_AUTH_TOKEN}" "https://${NGINX_XIVO_HOST}/configmgt/api/1.0/mediaserver/"); then
        echo "$ME: info: Failed to retrieve JSON data from https://${NGINX_XIVO_HOST}/configmgt/api/1.0/mediaserver/, exiting"
        exit 1
    fi
}

extract_mds_name_and_ip(){
    readarray -t mds_names < <(jq -r '.[] | select(.name != "default") | .name' <<< "$json_mds")
    readarray -t mds_ip < <(jq -r '.[] | select(.name != "default") | .voip_ip' <<< "$json_mds")
}

create_destination_file(){
    mkdir -p /etc/nginx/sip_proxy
    dest_file="/etc/nginx/sip_proxy/sip_proxy.conf"
}

create_nginx_config_for_each_mds(){
    content=""
    for index in $(seq 0 $(( ${#mds_names[@]}-1)) )
    do
        content+="location /wssip-${mds_names[$index]} {
    auth_request /validatetoken;

    proxy_pass http://${mds_ip[$index]}:5039/ws;
    include /etc/nginx/xivo/proxy-ws_params;
    include /etc/nginx/xivo/proxy_params;

    keepalive_timeout 180s;
}

"
    done
}

overwrite_destination_file(){
    echo "$content" > $dest_file
}

main() {
    verify_configmgt_access_provided

    fetch_json_mds_list

    extract_mds_name_and_ip

    create_nginx_config_for_each_mds

    create_destination_file

    overwrite_destination_file
    echo "$ME: info: Sip proxy configuration file written to $dest_file."
}

main